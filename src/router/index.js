import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: resolve => require(['@/components/login.vue'],resolve),
    },
    {
      path: '/index',
      name: 'index',
      redirect:'/business',
      component: resolve => require(['@/components/index.vue'],resolve),
      children:[
        {
          path: '/business',
          name: 'business',
          component: resolve => require(['@/components/business.vue'],resolve),
        },
        {
          path: '/busiDetail',
          name: 'busiDetail',
          component: resolve => require(['@/components/busiDetail.vue'],resolve),
        },
        {
          path: '/dobusi',//处理任务
          name: 'dobusi',
          component: resolve => require(['@/components/dobusi.vue'],resolve),
        },
        {
          path: '/myInfo',
          name: 'myInfo',
          component: resolve => require(['@/components/myInfo.vue'],resolve),
        },
        {
          path: '/myInfoDetail',//我的信息
          name: 'myInfoDetail',
          component: resolve => require(['@/components/myInfoDetail.vue'],resolve),
        },
        {
          path: '/changePwd',//修改密码
          name: 'changePwd',
          component: resolve => require(['@/components/changePwd.vue'],resolve),
        },
        {
          path: '/shop',//积分商城
          name: 'shop',
          component: resolve => require(['@/components/shop.vue'],resolve),
        },
        {
          path: '/goodsDetail',//商品详情
          name: 'goodsDetail',
          component: resolve => require(['@/components/goodsDetail.vue'],resolve),
        },
        {
          path: '/integralRanking',//积分榜
          name: 'integralRanking',
          component: resolve => require(['@/components/integralRanking.vue'],resolve),
        },
        {
          path: '/myRanking',//我的积分
          name: 'myRanking',
          component: resolve => require(['@/components/myRanking.vue'],resolve),
        },
        {
          path: '/logistics',//物流
          name: 'logistics',
          component: resolve => require(['@/components/Logistics.vue'],resolve),
        },
        {
          path: '/myAddress',//我的地址
          name: 'myAddress',
          component: resolve => require(['@/components/myAddress.vue'],resolve),
        },
        {
          path: '/taskList',//任务悬赏
          name: 'taskList',
          component: resolve => require(['@/components/taskList.vue'],resolve),
        },
        {
          path: '/taskDetail',//任务悬赏详情
          name: 'taskDetail',
          component: resolve => require(['@/components/taskDetail.vue'],resolve),
        },
      ]
    }
  ]
})
