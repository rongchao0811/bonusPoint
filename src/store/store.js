import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  token:'',
  userInfo:{},
  isEdit:false,//是否可编辑false不可 true可编辑
  detailId:'',//编辑页面需要携带的id
  systemNo:'',//系统编号
  menuNo:'',//菜单编号
  currentData:{},
  authority:'',//当前按钮权限级别 0最高
  loading2:false
}


const mutations = {
  setAuthoity(state,v){
    state.authority=v
  },
  changeMenu(state,v){
    state.menuNo=v
  },
  changeSys(state,v){
    state.systemNo=v
  },
  changeId(state,v){
    state.detailId=v
  },
  setCurrentData(state,v){
    state.currentData = v
  },
  changeEdit(state,v){
    state.isEdit=v
  },
  saveToken(state,t){
    state.token=t
    window.sessionStorage.setItem('token',t)
  },
  saveUserInfo(state,v){
    state.userInfo=v
  },
  setLoading(state,v){
    state.loading2=v
  }
}
const getters = {

}
const actions = {

}
export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})
