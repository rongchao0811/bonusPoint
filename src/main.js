// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import '../static/css/reset.css'
import axios from 'axios'
import vuex from 'vuex'
import store from './store/store'
import MuseUI from 'muse-ui';
import Toast from 'muse-ui-toast';
import Message from 'muse-ui-message';
import 'muse-ui/dist/muse-ui.css';

Vue.use(Message);

Vue.use(MuseUI);
Vue.use(Toast);
Vue.use(vuex)


Vue.config.productionTip = false
Vue.prototype.$ReqUrl = 'http://39.107.243.177/mobile/#/'

let serverUrl = 'http://39.107.243.177:8080';
Vue.prototype.$publicUrl = serverUrl//线上环境接口地址
//axios.defaults.headers.common['accessToken'] = token;
var instance = axios.create({
  baseURL: serverUrl,
});
Vue.prototype.$axios = instance;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
